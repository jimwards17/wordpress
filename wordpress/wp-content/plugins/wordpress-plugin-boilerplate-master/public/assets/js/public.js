(function ( $ ) {
	'use strict';

	$(function () {

		if($(window).width() > 1280){
			$('#picture1, #picture2').width($(document).width());
			$('#picture1, #picture2').height(($('#picture1').width()/1.3333));
		}

		$('#picture2').hide();
		$('.navbar a').css('color', 'white');
	});

	
	$(window).resize(function () {
		$('#picture1, #picture2').width($(document).width());
		$('#picture1, #picture2').height(($('#picture1').width()/1.3333));

	});



	$(window).scroll(function () {

		if($(window).scrollTop() > ($('#picture1').height() /1.6) ) {
			$('.navbar a').removeAttr('style');
			$('.navbar').css('background','white');
			$('.navbar').css('borderColor','transparent');
		}else{
			$('.navbar').removeAttr('style');
			$('.navbar a').removeAttr('style');
			$('.navbar a').css('color', 'white');

			if($('#myNavbar').hasClass('in')){
				$('#myNavbar a').removeAttr('style');

			}
		}

		if($(window).scrollTop() > $('#picture1').height()/1.4) {
			$('#picture1').hide();
			$('#picture2').show();
		}else{
			$('#picture1').show();
			$('#picture2').hide();
		}


		if($(window).scrollTop() + $(window).height() === $(document).height()) {
			$('#footer').css('backgroundColor','transparent');

		}

	});

	$('.navbar-toggle').click(function (){
		$('#myNavbar a').removeAttr('style');
	});

	$('.navbar a').click(function(event) {

		event.preventDefault();

		var hash = this.hash;

		$('html, body').animate({scrollTop: $(hash).offset().top}, 800, function(){
			//window.location.hash = hash;
		});
	});



}(jQuery));
