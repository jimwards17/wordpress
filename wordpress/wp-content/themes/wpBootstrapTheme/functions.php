<?php 


add_action('wp_head', 'mbe_wp_head');

add_filter('body_class', 'mbe_body_class');


function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap-3.3.6/dist/js/bootstrap.min.js', array( 'jquery' ) );

	// For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );


}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


function mbe_body_class($classes){
    if(is_user_logged_in()){
        $classes[] = 'body-logged-in';
    } else{
        $classes[] = 'body-logged-out';
    }
    return $classes;
}


function mbe_wp_head(){
    echo '<style>'.PHP_EOL;
    echo 'body{ padding-top: 70px !important; }'.PHP_EOL;
    // Using custom CSS class name.
    echo 'body.body-logged-in .navbar-fixed-top{ top: 28px !important; }'.PHP_EOL;
    // Using WordPress default CSS class name.
    echo 'body.logged-in .navbar-fixed-top{ top: 28px !important; }'.PHP_EOL;
    echo '</style>'.PHP_EOL;
}


?>