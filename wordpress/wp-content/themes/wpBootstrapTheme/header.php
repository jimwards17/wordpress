<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>    
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- style -->
  <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

      <!-- favicon -->
      <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/bootstrap-3.3.6/docs/Iconsmind-Outline-Administrator.ico" />

      <?php wp_head(); ?>

    </head>


    <?php echo '<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" class="'.join(' ', get_body_class()).'">'.PHP_EOL; ?>

    <nav class="navbar navbar-default navbar-fixed-top ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="#myPage">James Edwards</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">


          <?php
          wp_nav_menu( array('menu_class' => 'nav navbar-nav navbar-right'));
          ?>


        </div>
      </div>
    </nav>























